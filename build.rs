
fn main() {
    prost_build::compile_protos(&["proto/logentry.proto"],
                                &["proto/"]).expect("Couldn't generate proto files");
}