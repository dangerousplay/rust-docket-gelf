#![feature(custom_attribute)]
#[macro_use]
extern crate serde;

extern crate serde_json;
extern crate clap;
extern crate tokio;

#[macro_use]
extern crate prost;

#[macro_use]
extern crate log;

#[global_allocator]
static ALLOC: jemallocator::Jemalloc = jemallocator::Jemalloc;

use clap::{App};
use crate::plugin::server::DockerGelfPlugin;
use env_logger::Env;

mod plugin;
mod gelf;

fn main() {
    env_logger::from_env(Env::default().default_filter_or("debug")).init();

    let _matches = App::new("gelf-docker-logger-driver")
        .version("0.1.1")
        .about("Gelf Docker Logger Driver");

    DockerGelfPlugin::create_from_config().expect("Unable to start Docker Gelf Plugin");
}
