use tokio::io;
use tokio::codec::{Decoder};
use crate::plugin::driver::log_entry::LogEntry;
use bytes::{BytesMut, BigEndian, ByteOrder};
use crate::prost::Message;

pub struct ProtobufDecoder{}

impl ProtobufDecoder {
    pub fn new() -> Self {
        ProtobufDecoder {}
    }
}

impl Decoder for ProtobufDecoder {
    type Item = LogEntry;
    type Error = io::Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        if src.len() < 4 {
            return Ok(None);
        }

        let size = BigEndian::read_u32(&src[..4]) as usize;

        debug!("Buffer size: {}", size);

        if src.len() < size+4 {
            return Ok(None);
        }

        src.advance(4);

        debug!("Trying to parse: Size: {}", size);

        let message = src[..size].to_vec();
        src.advance(size);

        LogEntry::decode(message.to_vec()).map(|z| Some(z)).map_err(|z| io::Error::new(io::ErrorKind::InvalidData, z))
    }
}


mod test {
    use crate::plugin::driver::log_entry::LogEntry;
    
    
    use crate::prost::Message;
    use bytes::{BytesMut, BufMut};
    
    
    
    
    
    
    
    

    fn encode_to_bytes(entry: &LogEntry) -> BytesMut {
        let mut vect = Vec::<u8>::new();
        let _encoded = entry.encode(&mut vect);

        let mut buffer = BytesMut::with_capacity(vect.len()+4);

        buffer.put_u32_be(vect.len() as u32);
        buffer.put(vect);

        buffer
    }

    #[test]
    fn size_with_message() {
        let mut codec = ProtobufDecoder::new();

        let log = "Some log to decode, hehehe".as_bytes().to_vec();

        let log_entry2: LogEntry = LogEntry {
            source: "SOME_SOURCE".to_string(),
            time_nano: SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis() as i64,
            line: log,
            partial: false,
            partial_log_metadata: None
        };

        let mut encoded = encode_to_bytes(&log_entry2);
        let resut = codec.decode(&mut encoded);

        assert!(resut.is_ok());
        let found = resut.unwrap().expect("Should be decoded");

        assert_eq!(found, log_entry2);
        assert!(encoded.is_empty());
    }

    #[test]
    fn size_with_messages() {
        let mut codec = ProtobufDecoder::new();

        let log = "Some log to decode, hehehe".as_bytes().to_vec();

        let log_entry2: LogEntry = LogEntry {
            source: "SOME_SOURCE".to_string(),
            time_nano: SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis() as i64,
            line: log,
            partial: false,
            partial_log_metadata: None
        };

        let mut reuse = BytesMut::new();

        for _i in 0..50 {
            let encoded = encode_to_bytes(&log_entry2);

            reuse.extend_from_slice(encoded.as_ref());

            let resut = codec.decode(&mut reuse);

            assert!(resut.is_ok());
            let found = resut.unwrap().expect("Should be decoded");

            assert_eq!(found, log_entry2);
            assert!(reuse.is_empty());
        }
    }

    #[test]
    fn size_messages_with_file() {
        let input = std::fs::File::create("/tmp/test.fifo").unwrap();
        let input2 = input.try_clone().unwrap();

        let mut runtime = Runtime::new().unwrap();
        let codec = ProtobufDecoder::new();

        let producer = futures::lazy(move || tokio_file_unix::File::new_nb(input)
            .and_then(|z| z.into_io(&tokio::reactor::Handle::default()))
            .map(|z| BufWriter::new(z))
            .map(|mut z| {
                tokio::spawn(
                    Interval::new_interval(Duration::from_secs(1))
                        .for_each(move |_| {
                            let msg = encode_to_bytes(&LogEntry {
                                source: "SOME_SOURCE".to_string(),
                                time_nano: SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis() as i64,
                                line: "awdawd".as_bytes().to_vec(),
                                partial: false,
                                partial_log_metadata: None
                            });

                            let mut buffer = BytesMut::with_capacity(msg.len() + 4);

                            buffer.put_u32_be(msg.len() as u32);

                            buffer.extend_from_slice(msg.as_ref());

                            z.write(buffer.as_ref()).unwrap();
                            z.flush().unwrap();
                            Ok(())
                        })
                        .map_err(|_| ())
                );
            })).map_err(|_| ());

        let consume = futures::lazy(|| tokio_file_unix::File::new_nb(input2)
            .and_then(|z| z.into_reader(&tokio::reactor::Handle::default()))
            .map(|z| FramedRead::new(z, codec))
            .map(|z| {
                tokio::spawn(z.for_each(|z| {
                    info!("Got this line: '{:?}'", String::from_utf8(z.line).unwrap());
                    Ok(())
                }).map_err(|_z| ()));
            }));

        runtime.spawn(producer);
        runtime.block_on(consume).unwrap();

        runtime.run().unwrap();
    }
}
