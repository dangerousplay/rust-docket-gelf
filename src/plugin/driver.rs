use std::collections::HashMap;
use gelf::{Logger};
use std::fs::File;
use lockfree::map::Map;
use crate::plugin::protobuf_decoder::ProtobufDecoder;
use tokio::codec::FramedRead;
use actix_web::error::BlockingError;
use futures::{Future, Stream};



pub mod log_entry {
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct LogEntry {
        #[prost(string, tag="1")]
        pub source: std::string::String,
        #[prost(int64, tag="2")]
        pub time_nano: i64,
        #[prost(bytes, tag="3")]
        pub line: std::vec::Vec<u8>,
        #[prost(bool, tag="4")]
        pub partial: bool,
        #[prost(message, optional, tag="5")]
        pub partial_log_metadata: ::std::option::Option<PartialLogEntryMetadata>,
    }
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct PartialLogEntryMetadata {
        #[prost(bool, tag="1")]
        pub last: bool,
        #[prost(string, tag="2")]
        pub id: std::string::String,
        #[prost(int32, tag="3")]
        pub ordinal: i32,
    }
    //include!(concat!(env!("OUT_DIR"), "/docker.grpc.plugin.rs"));
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ContainerInfo {
    #[serde(rename = "Config")]
    pub config: Option<HashMap<String, String>>,

    #[serde(rename = "ContainerID")]
    pub container_id: Option<String>,

    #[serde(rename = "ContainerName")]
    pub container_name: Option<String>,

    #[serde(rename = "ContainerEntrypoint")]
    pub container_entrypoint: Option<String>,

    #[serde(rename = "ContainerArgs")]
    pub container_args: Option<Vec<String>>,

    #[serde(rename = "ContainerImageID")]
    pub container_image_id: Option<String>,

    #[serde(rename = "ContainerImageName")]
    pub container_image_name: Option<String>,

    #[serde(rename = "ContainerCreated")]
    pub container_created: Option<String>,

    #[serde(rename = "ContainerEnv")]
    pub container_env: Option<Vec<String>>,

    #[serde(rename = "ContainerLabels")]
    pub container_labels: Option<HashMap<String, String>>,

    #[serde(rename = "LogPath")]
    pub logpath: Option<String>,

    #[serde(rename = "DaemonName")]
    pub daemon_name: Option<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct StartLogRequest {

    #[serde(rename = "File")]
    pub file: String,

    #[serde(rename = "Info")]
    pub info: ContainerInfo
}


struct ContainerLoggerContext {
    file_path: String,
    file: File,
    container: ContainerInfo,
    logger: Logger,
}

impl ContainerLoggerContext {

}

pub struct Driver {
    container_context: Map<String, ContainerLoggerContext>,
}

impl Driver {
    pub fn new() -> Self {
        Driver { container_context: Map::new() }
    }

    pub fn start_consume(&self, request: StartLogRequest) {
        actix_rt::blocking::run(|| File::open(request.file).and_then(|z|tokio_file_unix::File::new_nb(z)))
            .and_then(|z| z.into_reader(&tokio::reactor::Handle::default()).map_err(|z| BlockingError::Error(z)))
            .map(|z| FramedRead::new(z, ProtobufDecoder::new()))
            .map(|z| {
                actix_rt::Arbiter::current().send(z.for_each(|_v| {
                    Ok(())
                }).map_err(|_z| ()));
            });


    }
}