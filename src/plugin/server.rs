
use tokio::io;
use std::fs;

use serde::{Deserialize, Serialize};
use std::sync::{Arc};
use futures::{Future, Stream};
use crate::plugin::driver::Driver;
use crate::plugin::driver::StartLogRequest;
use actix_web::{HttpServer, App, web, middleware, HttpResponse, Error};
use std::ops::Deref;
use actix_web::body::Body;
use tokio::codec::{FramedRead};
use crate::plugin::protobuf_decoder::{ProtobufDecoder};
use std::fs::File;

/// DefaultContentTypeV1_1 is the default content type accepted and sent by the plugins.
const PLUGIN_CONTENT_TYPE_V1_1: &'static str = "application/vnd.docker.plugins.v1.1+json";

/// The Path called to activate the Plugin
const ACTIVATE_PATH: &'static str = "/Plugin.Activate";

const CAPABILITIES_PATH: &'static str = "/LogDriver.Capabilities";

const LOG_DRIVER: &'static str = "LoggingDriver";

const START_LOGGING_PATH: &'static str = "/LogDriver.StartLogging";

const PLUGIN_PATH: &'static str = "/run/docker/plugins/";

const PLUGIN_SOCKET_PATH: &'static str = "/run/docker/plugins/gelf-driver.sock";

/// A representation of plugin implementations, like logger driver.
#[derive(Serialize, Deserialize)]
struct Plugin {
    #[serde(rename = "Implements")]
    implements: Vec<String>,
}

pub struct DockerGelfPlugin {
    plugin: Plugin
}

pub struct RouterService {
    plugin: Plugin,
    driver: Driver
}

impl Plugin {
    fn new(implementations: Vec<String>) -> Self {
        Plugin { implements: implementations }
    }
}

fn start_plugin(plugin: web::Data<Arc<Plugin>> ) -> HttpResponse {
    HttpResponse::Ok().content_type(PLUGIN_CONTENT_TYPE_V1_1).json2(plugin.deref())
}

fn start_logging(body: web::Payload) -> impl Future<Item = HttpResponse, Error = Error> {
    body.concat2().from_err()
        .and_then(|body| {
            let obj = serde_json::from_slice::<StartLogRequest>(&body).map_err(Error::from).unwrap();
            actix_rt::blocking::run(|| File::open(obj.file)).map_err(Error::from)
        })
        .and_then(|z| {
            tokio_file_unix::File::new_nb(z)
                .and_then(|z| z.into_reader(&tokio::reactor::Handle::default()))
                .map_err(Error::from)
        })
        .map(|z| FramedRead::new(z, ProtobufDecoder::new()))
        .map(|z| {
            actix_rt::Arbiter::current().send(z.for_each(|v| {
                info!("Got this line: '{:?}'", String::from_utf8(v.line).unwrap());
                Ok(())
            }).map_err(|_z| ()));
        })
        .and_then(|_body| Ok(HttpResponse::Ok().content_type("application/json").body(Body::from("{}"))))
        .map_err(Error::from)
}

impl DockerGelfPlugin {
    pub fn create_from_config() -> io::Result<()> {
        let sys = actix_rt::System::new("docker log driver");

        fs::create_dir_all(PLUGIN_PATH)?;

        HttpServer::new(move || {
            let plugin = Plugin::new(vec![LOG_DRIVER.to_string()]);
            let driver = Driver::new();

            App::new()
                .data(Arc::new(plugin))
                .data(Arc::new(driver))
                .wrap(middleware::Logger::default())
                .route(ACTIVATE_PATH, web::post().to(start_plugin))
                .route(START_LOGGING_PATH, web::post().to_async(start_logging))
                .route("*", web::to(|| HttpResponse::NotFound()))
        })
            .bind_uds(PLUGIN_PATH.to_owned() + "gelf-driver.sock")?
            .start();

        sys.run()?;

        Ok(())
    }


}