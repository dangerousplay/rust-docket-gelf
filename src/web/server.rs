use tokio::net::{UnixListener, TcpListener};
use tokio::io;
use std::fs;
use tokio::reactor::Handle;
use serde::{Deserialize, Serialize};
use std::sync::{Mutex, Arc};
use futures::Future;
use crate::plugin::driver::Driver;
use crate::plugin::driver::StartLogRequest;
use actix_web::{HttpServer, App, web, middleware, HttpResponse, Responder};
use std::borrow::Borrow;
use std::ops::Deref;
use actix_web::body::Body;

/// DefaultContentTypeV1_1 is the default content type accepted and sent by the plugins.
const PLUGIN_CONTENT_TYPE_V1_1: &'static str = "application/vnd.docker.plugins.v1.1+json";

/// The Path called to activate the Plugin
const ACTIVATE_PATH: &'static str = "/Plugin.Activate";

const LOG_DRIVER: &'static str = "LoggingDriver";

const START_LOGGING_PATH: &'static str = "/LogDriver.StartLogging";

const PLUGIN_PATH: &'static str = "/run/docker/plugins/";

const PLUGIN_SOCKET_PATH: &'static str = "/run/docker/plugins/gelf-driver.sock";

/// A representation of plugin implementations, like logger driver.
#[derive(Serialize, Deserialize)]
struct Plugin {
    #[serde(rename = "Implements")]
    implements: Vec<String>,
}

#[derive(Serialize, Deserialize)]
struct Error<'a> {
    #[serde(rename = "Err")]
    error: &'a str
}

pub struct DockerGelfPlugin {
    plugin: Plugin
}

pub struct RouterService {
    plugin: Plugin,
    driver: Driver
}

impl Plugin {
    fn new(implementations: Vec<String>) -> Self {
        Plugin { implements: implementations }
    }
}

fn start_plugin(plugin: web::Data<Arc<Plugin>>) -> HttpResponse {
    HttpResponse::Ok().content_type(PLUGIN_CONTENT_TYPE_V1_1).json(plugin.deref())
}

fn start_logging(plugin: web::Data<Arc<Plugin>>, context: web::Json<StartLogRequest>) -> HttpResponse {
    info!("Received log request: {:?}", context);

    HttpResponse::Ok().body(Body::Empty)
}

impl DockerGelfPlugin {
    pub fn create_from_config() -> io::Result<()> {
        let sys = actix_rt::System::new("docker log driver");

        fs::create_dir_all(PLUGIN_PATH)?;

        HttpServer::new(move || {
            let plugin = Plugin::new(vec![LOG_DRIVER.to_string()]);
            let driver = Driver::new();

            App::new()
                .data(Arc::new(plugin))
                .data(Arc::new(driver))
                .wrap(middleware::Logger::default())
                .route(ACTIVATE_PATH, web::post().to(start_plugin))
                .route(START_LOGGING_PATH, web::post().to(start_logging))
        })
            .bind_uds(PLUGIN_SOCKET_PATH)?
            .start();

        sys.run()?;

        Ok(())
    }


}