FROM alpine

WORKDIR /usr/bin

COPY target/x86_64-unknown-linux-musl/debug/docker-log-driver-gelf .

RUN chmod +x ./docker-log-driver-gelf

ENTRYPOINT ["/usr/bin/docker-log-driver-gelf"]